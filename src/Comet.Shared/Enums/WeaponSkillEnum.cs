﻿namespace Comet.Shared.Enums
{
    public enum WeaponSkillEnum
    {
        Boxing,
        Blade = 410,
        Sword = 420,
        BackSword = 421,
        Hook = 430,
        Whip = 440,
        Axe = 450,
        Hammer = 460,
        Club = 480,
        Scepter = 481,
        Dagger = 490,
        Bow = 500,
        Glaive = 510,
        Poleaxe = 530,
        LongHammer = 540,
        Spear = 560,
        Wand = 561,
        Halbert = 580,
        Katana = 601,
        PrayerBeads = 610,
        Rapier = 611,
        Pistol = 612,
    }
}
