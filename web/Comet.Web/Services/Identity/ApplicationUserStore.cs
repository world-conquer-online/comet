﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Comet.Web.Data;
using Comet.Web.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Comet.Web.Services.Identity
{
    public class ApplicationUserStore : UserStore<
        ApplicationUser,
        ApplicationRole,
        ApplicationDbContext,
        int,
        ApplicationUserClaim,
        ApplicationUserRole,
        ApplicationUserLogin,
        ApplicationUserToken,
        ApplicationRoleClaim
    >
    {
        private readonly LanguageService mLanguageService;
        private readonly ILogger<ApplicationUserStore> mLogger;

        public ApplicationUserStore(ILogger<ApplicationUserStore> logger,
                                    ApplicationDbContext dbContext,
                                    LanguageService language)
            : base(dbContext)
        {
            mLogger = logger;
            mLanguageService = language;
        }

        public override async Task<IdentityResult> CreateAsync(ApplicationUser user,
                                                               CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            try
            {
                if (await Context.Users.AnyAsync(usr => usr.NormalizedEmail.Equals(user.NormalizedEmail),
                                                 cancellationToken))
                    return IdentityResult.Failed(new IdentityError
                    {
                        Code = "EMAIL_IN_USE", Description = mLanguageService.GetString("EmailInUse")
                    });

                if (await Context.Users.AnyAsync(usr => usr.NormalizedUserName.Equals(user.NormalizedUserName),
                                                 cancellationToken))
                    return IdentityResult.Failed(new IdentityError
                    {
                        Code = "USERNAME_ALREADY_EXISTS", Description = mLanguageService.GetString("UserNameInUse")
                    });

                Context.Users.Update(user);
                await Context.SaveChangesAsync(cancellationToken);
                return IdentityResult.Success;
            }
            catch (Exception ex)
            {
                mLogger.LogError(ex.ToString());
                return IdentityResult.Failed(new IdentityError
                {
                    Code = ex.HResult.ToString("X08"), Description = ex.InnerException.Message
                });
            }
        }
    }
}