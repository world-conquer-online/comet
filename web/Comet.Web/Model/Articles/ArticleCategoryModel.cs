﻿namespace Comet.Web.Model.Articles
{
    public class ArticleCategoryModel
    {
        public int Id { get; set; }
        public string RealName { get; set; }
        public string Name { get; set; }
    }
}