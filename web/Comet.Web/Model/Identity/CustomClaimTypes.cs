﻿namespace Comet.Web.Model.Identity
{
    public class CustomClaimTypes
    {
        public const string Language = "http://schemas.worldconquer.online/ws/2022/03/identity/claims/locale";
        public const string SocialName = "http://schemas.worldconquer.online/ws/2022/03/identity/claims/socialname";
        public const string Pronoum = "http://schemas.worldconquer.online/ws/2022/03/identity/claims/pronoum";

        public const string SelfDescription =
            "http://schemas.worldconquer.online/ws/2022/03/identity/claims/selfdescription";
    }
}