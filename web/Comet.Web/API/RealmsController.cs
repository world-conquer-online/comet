﻿using Microsoft.AspNetCore.Mvc;

namespace Comet.Web.API
{
    [ApiVersion("1")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RealmsController : ControllerBase
    {
    }
}