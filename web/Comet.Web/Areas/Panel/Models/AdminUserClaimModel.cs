﻿namespace Comet.Web.Areas.Panel.Models
{
    public class AdminUserClaimModel
    {
        public int Id { get; set; }
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
        public string ClaimName { get; set; }
    }
}