﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace Comet.Web.Data.Entities
{
    [Table("account_user_claims")]
    public class ApplicationUserClaim : IdentityUserClaim<int>
    {
        [Key]
        public override int Id
        {
            get => base.Id;
            set => base.Id = value;
        }

        [ForeignKey("FK_AspNetUserClaims_AspNetUsers_UserId")]
        public override int UserId { get; set; }
    }
}